<?php

define('HOOKS', []);

function add_hook(string $name, $function) {
	global $HOOKS;
	$HOOKS[$name][] = $function;
}

function fire_hook(string $name, $data) {
	global $HOOKS;
	if ($HOOKS[$name]) foreach ($HOOKS[$name] as $hook) {
		$hook($data);
	}
}

// -- System hooks
